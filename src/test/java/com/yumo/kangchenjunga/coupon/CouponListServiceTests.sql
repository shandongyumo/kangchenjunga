-- delete from coupon_pool where coupon_id between 100 and 199;
-- delete from coupon_customer_allocation where coupon_id between 100 and 199;
-- delete from coupon_customer_obtainment where coupon_id between 100 and 199;
-- delete from activity where activity_id in (1, 2);

insert into activity (activity_id, title, start_date, end_date, customer_levels, quantity, price)
  values (1, '中秋活动', '2019-01-01', '2019-12-31', '1', 3, 1.00);
insert into activity (activity_id, title, start_date, end_date, customer_levels, quantity, price)
  values (2, '元旦活动', '2019-01-01', '2019-12-31', '2', 2, 2.00);

insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (100, 1, '12345600', '123456', 1440, 'GENERATED', current_timestamp(), 'ALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (101, 2, '12345601', '123456', 1440, 'GENERATED', current_timestamp(), 'INVALID');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (102, 2, '12345602', '123458', 1440, 'GENERATED', current_timestamp(), 'ALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (103, 1, '12345603', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (104, 2, '12345604', '123457', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (105, 1, '12345605', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (106, 1, '12345606', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (107, 1, '12345607', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (108, 2, '12345608', '123456', 1440, 'IMPORTED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (109, 2, '12345609', '123456', 1440, 'IMPORTED', current_timestamp(), 'ALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (110, 1, '12345610', '123456', 1440, 'IMPORTED', current_timestamp(), 'ALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (111, 1, '12345611', '123456', 1440, 'IMPORTED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (112, 1, '12345612', '123456', 1440, 'IMPORTED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (113, 1, '12345613', '123456', 1440, 'IMPORTED', current_timestamp(), 'UNALLOCATED');

