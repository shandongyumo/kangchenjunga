delete from activity where activity_id = 1;
insert into activity (activity_id, title, start_date, end_date, customer_levels, quantity, price)
  values (1, '中秋活动', '2019-01-01', '2019-12-31', '1', 3, 1.00);

delete from customer where customer_id = 1;
insert into customer (customer_id, customer_number, customer_level, nickname, phone)
  values (1, 'XT20161026', 1, '极光6', '13512345678');

delete from coupon_customer_allocation where coupon_id in (100, 101);
delete from coupon_customer_obtainment where coupon_id = 101;
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 100, 1, '12345600', '123456', 1440, current_timestamp(), 'OBTAINED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 101, 1, '12345601', '123456', 1440, current_timestamp(), 'ALLOCATED');
