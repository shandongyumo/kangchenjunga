package com.yumo.kangchenjunga.coupon;

import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;

import com.yumo.kangchenjunga.coupon.v1.CouponListItemVo;
import com.yumo.kangchenjunga.coupon.v1.CouponListService;

@SpringBootTest
@Sql
@Transactional
public final class CouponListServiceTests {

	@Autowired
	private CouponListService service;

	@Test
	public void testPoolPage() {
		var result = service.poolPage(PageRequest.of(0, 6));

		assertThat(result).hasSize(6);

		result = service.poolPage(PageRequest.of(1, 6));

		assertThat(result).hasSize(3);
	}

	@Test
	public void testPoolQuery() {
		var query = new CouponListItemVo();

		var result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(6);
		assertThat(result.getTotalElements()).isEqualTo(9);

		result = service.poolQuery(query, PageRequest.of(1, 6));

		assertThat(result).hasSize(3);
	}

	@Test
	public void testPoolQueryUseActivityId() {
		var query = new CouponListItemVo();

		query.activityId = 1;

		var result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(6);
		assertThat(result.getTotalElements()).isEqualTo(7);
	}

	@Test
	public void testPoolQueryUseCouponCode() {
		var query = new CouponListItemVo();

		// 未发放的码
		query.couponCode = "12345606";

		var result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(1);
		assertThat(result.getTotalElements()).isEqualTo(1);

		// 已发放的码
		query.couponCode = "12345609";

		result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(0);
		assertThat(result.getTotalElements()).isEqualTo(0);

		// 已作废的码
		query.couponCode = "12345601";

		result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(0);
		assertThat(result.getTotalElements()).isEqualTo(0);
	}

	@Test
	public void testPoolQueryUseSecretCode() {
		var query = new CouponListItemVo();

		query.secretCode = "123456";

		var result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(6);
		assertThat(result.getTotalElements()).isEqualTo(8);
	}

	@Test
	public void testPoolQueryUseActivityIdAndSecretCode() {
		var query = new CouponListItemVo();

		query.activityId = 1;
		query.secretCode = "123456";

		var result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(6);
		assertThat(result.getTotalElements()).isEqualTo(7);

		query.activityId = 2;
		query.secretCode = "123457";

		result = service.poolQuery(query, PageRequest.of(0, 6));

		assertThat(result).hasSize(1);
		assertThat(result.getTotalElements()).isEqualTo(1);
	}

}
