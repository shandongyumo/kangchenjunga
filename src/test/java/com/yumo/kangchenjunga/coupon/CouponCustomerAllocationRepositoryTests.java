package com.yumo.kangchenjunga.coupon;

import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import com.yumo.kangchenjunga.coupon.v1.CouponCustomerAllocationRepository;

@DataJpaTest(properties = { "spring.test.database.replace=none",
		"spring.datasource.url=jdbc:mariadb://localhost/devkit", "spring.datasource.password=secret",
		"spring.datasource.username=devkit" })
@Transactional
public final class CouponCustomerAllocationRepositoryTests {

	@Autowired
	private CouponCustomerAllocationRepository repository;

	@Sql
	@Test
	public void testSummaryByCustomerId() {
		var list = repository.summaryByCustomerId(1);

		assertThat(list).hasSize(4);
	}

}
