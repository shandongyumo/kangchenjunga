package com.yumo.kangchenjunga.coupon;

import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;

import com.yumo.kangchenjunga.activity.NoSuchActivityException;
import com.yumo.kangchenjunga.coupon.v1.CouponService;

@SpringBootTest
@Transactional
public class CouponServiceTests {

	@Autowired
	private CouponService service;

	@Sql
	@Test
	@WithUserDetails
	@Disabled
	public void testAllocateByCustomerLevel() throws NoSuchActivityException {
		var result = service.allocateByCustomerLevel(1, 1, 3);

		assertThat(result.couponAmount).isEqualTo(6);
		assertThat(result.couponPerCustomer).isEqualTo(3);
		assertThat(result.customerAmount).isEqualTo(2);
	}

}
