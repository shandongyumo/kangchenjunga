insert into company (company_id, name, contacts, phone, address)
  values (1, 'default', 'default', 'default', 'default');

insert into user (username, salt, password, company_id, privilege)
  values ('user', '3e95a635-89d8-41f6-baa5-e5e2170eacee', '52bccbac75eea7a5053166ba4044a38d039319e06dcacfd401ed54684f49cb48', 1, 'admin');

insert into activity (activity_id, title, company_id, start_date, end_date, customer_levels, quantity, price)
  values (1, '中秋活动', 1, '2019-01-01', '2019-12-31', '1', 3, 1.00);

insert into customer (customer_number, customer_level, nickname, phone)
  values ('XT20161026', 1, '极光6', '13512345678');
insert into customer (customer_number, customer_level, nickname, phone)
  values ('XT20161027', 1, '极光7', '13512345679');

insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345600', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345601', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345602', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345603', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345604', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345605', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345606', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345607', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345608', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
insert into coupon_pool (`activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `create_type`, `create_time`, `status`)
  values (1, '12345609', '123456', 1440, 'GENERATED', current_timestamp(), 'UNALLOCATED');
