delete from coupon_customer_allocation where coupon_id between 101 and 299;
delete from coupon_customer_obtainment where coupon_id between 101 and 299;

-- 客户 1，活动 1，未领 2，已领 2，已用 2
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 101, 1, '12345600', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 102, 1, '12345601', '123456', 1440, current_timestamp(), 'ALLOCATED');

insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 103, 1, '12345602', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 104, 1, '12345603', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 103, 1, '12345602', '123456', current_timestamp(), 'OBTAINED', current_timestamp(), current_timestamp());
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 104, 1, '12345603', '123456', current_timestamp(), 'OBTAINED', current_timestamp(), current_timestamp());

insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 105, 1, '12345604', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 106, 1, '12345605', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 105, 1, '12345604', '123456', current_timestamp(), 'USED', current_timestamp(), current_timestamp());
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 106, 1, '12345605', '123456', current_timestamp(), 'USED', current_timestamp(), current_timestamp());

-- 客户 1，活动 2，未领 2，已领 0，已用 0
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 107, 2, '12345606', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 108, 2, '12345607', '123456', 1440, current_timestamp(), 'ALLOCATED');

-- 客户 1，活动 3，未领 0，已领 2，已用 0
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 109, 3, '12345608', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 110, 3, '12345609', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 109, 3, '12345608', '123456', current_timestamp(), 'OBTAINED', current_timestamp(), current_timestamp());
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 110, 3, '12345609', '123456', current_timestamp(), 'OBTAINED', current_timestamp(), current_timestamp());

-- 客户 1，活动 4，未领 0，已领 0，已用 2
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 111, 4, '12345610', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (1, 112, 4, '12345611', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 111, 4, '12345610', '123456', current_timestamp(), 'USED', current_timestamp(), current_timestamp());
insert into coupon_customer_obtainment (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `obtain_time`, `status`, `effective_time`, `expiration_time`)
  values (1, 112, 4, '12345611', '123456', current_timestamp(), 'USED', current_timestamp(), current_timestamp());

-- 客户 2，活动 1，未领 2，已领 0，已用 0
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (2, 201, 1, '12345612', '123456', 1440, current_timestamp(), 'ALLOCATED');
insert into coupon_customer_allocation (`customer_id`, `coupon_id`, `activity_id`, `coupon_code`, `secret_code`, `term_by_minute`, `allocate_time`, `status`)
  values (2, 202, 1, '12345613', '123456', 1440, current_timestamp(), 'ALLOCATED');
