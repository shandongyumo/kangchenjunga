package com.yumo.kangchenjunga.customer;

import org.apache.commons.lang3.builder.ToStringBuilder;

public final class CustomerVo {

	public Integer id;
	public String customerNumber;
	public String nickname;
	public String phone;
	public Integer customerLevel;

	public CustomerVo() {
		// Empty.
	}

	public CustomerVo(Customer customer) {
		this.id = customer.getId();
		this.customerNumber = customer.customerNumber;
		this.nickname = customer.nickname;
		this.phone = customer.phone;
		this.customerLevel = customer.customerLevel;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
