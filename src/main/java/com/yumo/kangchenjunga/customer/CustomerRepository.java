package com.yumo.kangchenjunga.customer;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	Iterable<Customer> findByCustomerLevelIn(Collection<Integer> customerLevels);

	List<Customer> findByCustomerLevel(int customerLevel);

	List<Customer> findByIdIn(Collection<Integer> customerIds);

}
