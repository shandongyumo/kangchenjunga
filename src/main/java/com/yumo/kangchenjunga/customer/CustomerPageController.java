package com.yumo.kangchenjunga.customer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/customers")
public class CustomerPageController {

	@Autowired
	private CustomerService service;

	@GetMapping("")
	public String page(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CustomerVo customer = new CustomerVo();
		customer.customerNumber = StringUtils.isBlank(request.getParameter("customerNumber")) ? null
				: request.getParameter("customerNumber");
		customer.nickname = StringUtils.isBlank(request.getParameter("nickname")) ? null
				: request.getParameter("nickname");
		customer.phone = StringUtils.isBlank(request.getParameter("phone")) ? null : request.getParameter("phone");
		customer.customerLevel = StringUtils.isBlank(request.getParameter("customerLevel")) ? null
				: Integer.valueOf(request.getParameter("customerLevel"));
		model.addAttribute("customers", service.query(customer, pageable));
		model.addAttribute("search", customer);
		return "admin/customer";
	}

}
