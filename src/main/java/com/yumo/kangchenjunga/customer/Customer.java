package com.yumo.kangchenjunga.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "customer")
public class Customer {

	@Column(name = "customer_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;
	public String customerNumber;
	public String nickname;
	public String phone;
	public Integer customerLevel;

	public int getId() {
		return id;
	}

	public static Customer create(CustomerRepository repository, Customer customer) {
		customer.id = 0;

		return repository.save(customer);
	}

	public Customer() {
		// Empty.
	}

	public Customer(CustomerVo vo) {
		this.id = vo.id;
		this.customerNumber = vo.customerNumber;
		this.nickname = vo.nickname;
		this.phone = vo.phone;
		this.customerLevel = vo.customerLevel;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
