package com.yumo.kangchenjunga.settlement;

import java.util.List;

public final class StatementVo {

	public final StatementEntryEntity entry;
	public final List<StatementDetailVo> details;

	public StatementVo(StatementEntryEntity entry, List<StatementDetailVo> details) {
		this.entry = entry;
		this.details = details;
	}

}
