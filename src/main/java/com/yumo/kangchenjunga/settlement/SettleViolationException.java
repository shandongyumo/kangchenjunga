package com.yumo.kangchenjunga.settlement;

public final class SettleViolationException extends Exception {

	private static final long serialVersionUID = -7110965669494177078L;

	public SettleViolationException(String message) {
		super(message);
	}

}
