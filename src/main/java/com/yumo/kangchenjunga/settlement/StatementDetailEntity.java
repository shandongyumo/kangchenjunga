package com.yumo.kangchenjunga.settlement;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "StatementDetail")
@Table(name = "statement_detail")
public class StatementDetailEntity {

	@Column(name = "statement_detail_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;

	public int statementEntryId;
	public int storeId;
	public String storeName;
	public int activityId;
	public String activityTitle;
	public BigDecimal price;
	public BigDecimal quantity;
	public BigDecimal amount;

	public StatementDetailEntity(int statementEntryId, int storeId, String storeName, int activityId,
			String activityTitle, BigDecimal price, BigDecimal quantity, BigDecimal amount) {
		this.statementEntryId = statementEntryId;
		this.storeId = storeId;
		this.storeName = storeName;
		this.activityId = activityId;
		this.activityTitle = activityTitle;
		this.price = price;
		this.quantity = quantity;
		this.amount = amount;
	}

}
