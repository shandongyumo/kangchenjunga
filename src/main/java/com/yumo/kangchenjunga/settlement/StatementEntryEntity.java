package com.yumo.kangchenjunga.settlement;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "StatementEntry")
@Table(name = "statement_entry")
public class StatementEntryEntity {

	@Column(name = "statement_entry_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;

	public int companyId;
	public String companyName;
	public Instant createTime;
	public LocalDate startDate;
	public LocalDate endDate;
	public BigDecimal amount;

	public int getId() {
		return id;
	}
	
	public StatementEntryEntity() {
		
	}

	public StatementEntryEntity(int companyId, String companyName, Instant createTime, LocalDate startDate,
			LocalDate endDate, BigDecimal amount) {
		this.companyId = companyId;
		this.companyName = companyName;
		this.createTime = createTime;
		this.startDate = startDate;
		this.endDate = endDate;
		this.amount = amount;
	}

}
