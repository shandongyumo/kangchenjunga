package com.yumo.kangchenjunga.settlement;

import java.math.BigDecimal;

public final class StatementDetailVo {

	public final int companyId;
	public final String companyName;
	public final int storeId;
	public final String storeName;
	public final int activityId;
	public final String activityTitle;
	public final BigDecimal price;
	public final BigDecimal quantity;
	public final BigDecimal amount;

	public StatementDetailVo(int companyId, String companyName, int storeId, String storeName, int activityId,
			String activityTitle, BigDecimal price, BigDecimal quantity, BigDecimal amount) {
		this.companyId = companyId;
		this.companyName = companyName;
		this.storeId = storeId;
		this.storeName = storeName;
		this.activityId = activityId;
		this.activityTitle = activityTitle;
		this.price = price;
		this.quantity = quantity;
		this.amount = amount;
	}

}
