package com.yumo.kangchenjunga.settlement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "InvoiceInfo")
@Table(name = "invoice_info")
public final class InvoiceInfoEntity {

	@Column(updatable = false)
	@Id
	private int id = 1;

	public String type;
	public String name;
	public String taxNo;
	public String addressAndPhone;
	public String branchAndAccount;
	public String title;

	public int getId() {
		return id;
	}

	public void save(InvoiceInfoRepository repository) {
		this.id = 1;

		repository.save(this);
	}

}
