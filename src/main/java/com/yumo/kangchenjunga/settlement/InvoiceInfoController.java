package com.yumo.kangchenjunga.settlement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/invoice-info/")
public final class InvoiceInfoController {

	@Autowired
	private InvoiceInfoRepository repository;

	@GetMapping
	public InvoiceInfoEntity get() {
		return repository.findById(1).orElseThrow();
	}

	@PutMapping
	public void modify(@RequestBody InvoiceInfoEntity entity) {
		entity.save(repository);
	}

}
