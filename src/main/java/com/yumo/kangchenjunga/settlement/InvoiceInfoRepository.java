package com.yumo.kangchenjunga.settlement;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceInfoRepository extends JpaRepository<InvoiceInfoEntity, Integer> {

}
