package com.yumo.kangchenjunga.settlement;

public interface StatementDetailProjection {

	int getCompanyId();

	String getCompanyName();

	int getStoreId();

	String getStoreName();

	int getActivityId();

	String getActivityTitle();

	int getQuantity();

}
