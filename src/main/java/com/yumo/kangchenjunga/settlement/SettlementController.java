package com.yumo.kangchenjunga.settlement;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yumo.kangchenjunga.company.NoSuchCompanyException;
import com.yumo.kangchenjunga.exception.controller.HttpBadRequestException;
import com.yumo.kangchenjunga.exception.controller.HttpNotFoundException;

@RestController
@RequestMapping("/api/v1/statements")
public final class SettlementController {

	@Autowired
	private SettlementService service;

	@GetMapping("/companies/{id}/settleStartDate")
	public LocalDate settleStartDate(@PathVariable int id) {
		return service.settleStartDate(id);
	}

	@GetMapping("/companies/{id}/defaultSettleEndDate")
	public LocalDate defaultSettleEndDate(@PathVariable int id) {
		return service.defaultSettleEndDate(id);
	}

	@PostMapping("/companies/{companyId}/settlePreview/")
	public StatementVo settlePreview(@PathVariable int companyId,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate endDate)
			throws HttpNotFoundException, HttpBadRequestException {
		try {
			return service.settlePreview(companyId, startDate, endDate);
		} catch (NoSuchCompanyException e) {
			throw HttpNotFoundException.build(e, "企业 [ id: %d ] 不存在。", companyId);
		} catch (SettleViolationException e) {
			throw HttpBadRequestException.build(e, e.getMessage());
		}
	}

	@PostMapping("/companies/{companyId}/settle/")
	public StatementVo settle(@PathVariable int companyId,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate endDate)
			throws HttpNotFoundException, HttpBadRequestException {
		try {
			return service.settle(companyId, startDate, endDate);
		} catch (NoSuchCompanyException e) {
			throw HttpNotFoundException.build(e, "企业 [ id: %d ] 不存在。", companyId);
		} catch (SettleViolationException e) {
			throw HttpBadRequestException.build(e, e.getMessage());
		}
	}

	@GetMapping("/companies/{companyId}/settlePreview/export")
	public void exportSettlePreview(@PathVariable int companyId,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate endDate, HttpServletRequest request,
			HttpServletResponse response) throws IOException, HttpNotFoundException, HttpBadRequestException {
		try {
			String filename = service.genExportFilename(companyId, startDate, endDate);

			response.setContentType("application/force-download");
			response.addHeader("Content-Disposition", String.format("attachment;fileName=\"%s\"",
					new String(filename.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1)));
			service.exportSettlePreview(companyId, startDate, endDate, response.getOutputStream());
		} catch (NoSuchCompanyException e) {
			throw HttpNotFoundException.build(e, "企业 [ id: %d ] 不存在。", companyId);
		} catch (SettleViolationException e) {
			throw HttpBadRequestException.build(e, e.getMessage());
		}
	}

}
