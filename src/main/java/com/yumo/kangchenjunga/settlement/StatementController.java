package com.yumo.kangchenjunga.settlement;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/statements")
public final class StatementController {

	@Autowired
	private StatementService service;

	@GetMapping("/companies/{companyId}/statements/all")
	public Map<String, Object> all(@PathVariable int companyId) {
		return service.queryStatement(companyId);
	}

}
