package com.yumo.kangchenjunga.settlement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yumo.kangchenjunga.company.CompanyService;

@Controller
@RequestMapping("")
public class SettlementPageController {
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private InvoiceInfoRepository invoiceInfoRepository;
	
	@GetMapping("/admin/settlements")
	public String adminSettlements(Model model) {
		model.addAttribute("companies", companyService.list());
		return "admin/settlement";
	}
	
	@GetMapping("/company/settlements")
	public String compnaySettlements() {
		return "company/settlement";
	}
	
	@GetMapping("/admin/statements")
	public String admimStatement(Model model) {
		model.addAttribute("companies", companyService.list());
		return "admin/statement";
	}
	
	@GetMapping("/company/statements")
	public String companyStatement() {
		return "company/statement";
	}
	
	@GetMapping("/admin/invoice")
	public String adminInvoice(Model model) {
		model.addAttribute("invoice", invoiceInfoRepository.findById(1).orElse(new InvoiceInfoEntity()));
		return "admin/invoice";
	}
	
	@GetMapping("/company/invoice")
	public String companyInvoice(Model model) {
		model.addAttribute("invoice", invoiceInfoRepository.findById(1).orElse(new InvoiceInfoEntity()));
		return "company/invoice";
	}
	
}
