package com.yumo.kangchenjunga.sms;

import java.time.Instant;

public final class VerificationCodeVo {

	public final String key;
	public final String serial;
	public final String code;
	public final Instant generated;
	public final Instant expired;

	public VerificationCodeVo(String key, String serial, String code, Instant generated, Instant expired) {
		this.key = key;
		this.serial = serial;
		this.code = code;
		this.generated = generated;
		this.expired = expired;
	}

}
