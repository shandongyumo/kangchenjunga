package com.yumo.kangchenjunga.company;

import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;

public final class CompanyVo {

	public Integer id;
	public String name;
	public CompanyCategoryVo category;
	public String contacts;
	public String phone;
	public String address;
	public String remark;

	public CompanyVo() {
		// Empty.
	}

	public CompanyVo(CompanyEntity entity) {
		this.id = entity.getId();
		this.name = entity.name;
		this.category = Optional.ofNullable(entity.category).map(CompanyCategoryVo::new).orElse(null);
		this.contacts = entity.contacts;
		this.phone = entity.phone;
		this.address = entity.address;
		this.remark = entity.remark;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
