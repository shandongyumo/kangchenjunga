package com.yumo.kangchenjunga.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity(name = "Store")
@Table(name = "store")
public final class StoreEntity {

	@Column(name = "store_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;

	public Integer companyId;
	public String name;
	public String contactInfo;
	public String address;

	public StoreEntity() {
		// Empty.
	}

	public StoreEntity(StoreVo store) {
		this.id = store.id;
		this.companyId = store.companyId;
		this.name = store.name;
		this.contactInfo = store.contactInfo;
		this.address = store.address;
	}

	public static StoreEntity create(StoreRepository repository, StoreEntity store) {
		store.id = 0;
		return repository.save(store);
	}

	public Integer getId() {
		return id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
