package com.yumo.kangchenjunga.company;

import com.yumo.kangchenjunga.common.Status;

public final class CompanyCategoryVo {

	public Integer id;
	public String name;
	public Status status;

	public CompanyCategoryVo() {
		// Empty.
	}

	public CompanyCategoryVo(CompanyCategoryEntity entity) {
		this.id = entity.getId();
		this.name = entity.name;
		this.status = entity.status;
	}

	public CompanyCategoryVo(Integer id, String name, Status status) {
		this.id = id;
		this.name = name;
		this.status = status;
	}

}
