package com.yumo.kangchenjunga.company;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<StoreEntity, Integer> {

	List<StoreEntity> findStoreByCompanyIdOrderByName(int companyId);

}
