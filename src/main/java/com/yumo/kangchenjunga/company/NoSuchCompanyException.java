package com.yumo.kangchenjunga.company;

public final class NoSuchCompanyException extends Exception {

	private static final long serialVersionUID = -4233615775373212388L;

	public NoSuchCompanyException(String message) {
		super(message);
	}

}
