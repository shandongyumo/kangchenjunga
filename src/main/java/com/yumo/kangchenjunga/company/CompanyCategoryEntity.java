package com.yumo.kangchenjunga.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.yumo.kangchenjunga.common.Status;

@Entity(name = "CompanyCategory")
@Table(name = "company_category")
public class CompanyCategoryEntity {

	@Column(name = "company_category_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;

	public String name;

	@Enumerated(EnumType.STRING)
	public Status status = Status.VALID;

	public CompanyCategoryEntity() {
		// Empty.
	}

	public CompanyCategoryEntity(CompanyCategoryVo vo) {
		this.id = vo.id;
		this.name = vo.name;
		this.status = vo.status;
	}

	public Integer getId() {
		return id;
	}

	public static CompanyCategoryEntity create(CompanyCategoryRepository repository, CompanyCategoryEntity entity) {
		entity.id = 0;
		return repository.save(entity);
	}

}
