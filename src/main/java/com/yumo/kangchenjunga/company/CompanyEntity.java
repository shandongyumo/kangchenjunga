package com.yumo.kangchenjunga.company;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity(name = "Company")
@Table(name = "company")
public class CompanyEntity {

	@Column(name = "company_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;

	public String name;
	@OneToOne
	@JoinColumn(name = "category")
	public CompanyCategoryEntity category;
	public String contacts;
	public String phone;
	public String address;
	public String remark;

	public CompanyEntity() {
		// Empty.
	}

	public CompanyEntity(CompanyVo company) {
		this.id = company.id;
		this.name = company.name;
		this.category = Optional.ofNullable(company.category).map(CompanyCategoryEntity::new).orElse(null);
		this.contacts = company.contacts;
		this.phone = company.phone;
		this.address = company.address;
		this.remark = company.remark;
	}

	public static CompanyEntity create(CompanyRepository repository, CompanyEntity company) {
		company.id = 0;
		return repository.save(company);
	}

	public Integer getId() {
		return id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
