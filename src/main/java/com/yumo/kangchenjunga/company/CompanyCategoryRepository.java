package com.yumo.kangchenjunga.company;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.yumo.kangchenjunga.common.Status;

public interface CompanyCategoryRepository extends JpaRepository<CompanyCategoryEntity, Integer> {

	List<CompanyCategoryEntity> findByStatus(Status status);

	Page<CompanyCategoryEntity> findByStatus(Status status, Pageable pageable);

}
