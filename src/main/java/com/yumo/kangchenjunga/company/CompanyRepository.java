package com.yumo.kangchenjunga.company;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<CompanyEntity, Integer> {

	List<CompanyEntity> findByIdGreaterThan(int i);

	Page<CompanyEntity> findByIdGreaterThan(int i, Pageable pageable);

	List<CompanyEntity> findByCategory(CompanyCategoryEntity entity);

}
