package com.yumo.kangchenjunga.company;

import org.apache.commons.lang3.builder.ToStringBuilder;

public final class StoreVo {

	public int id;
	public int companyId;
	public String name;
	public String contactInfo;
	public String address;

	public StoreVo() {
		// Empty.
	}

	public StoreVo(StoreEntity entity) {
		this.id = entity.getId();
		this.name = entity.name;
		this.companyId = entity.companyId;
		this.contactInfo = entity.contactInfo;
		this.address = entity.address;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
