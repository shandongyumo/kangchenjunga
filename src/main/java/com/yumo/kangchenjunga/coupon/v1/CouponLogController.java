package com.yumo.kangchenjunga.coupon.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/coupons/logs")
public final class CouponLogController {

	@Autowired
	private CouponLogService service;

	@GetMapping(path = "/", params = { "paged" })
	public Page<CouponLogEntity> page(
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		return service.page(pageable);
	}

	@GetMapping("/service/query")
	public Page<CouponLogEntity> query(@RequestBody CouponLogEntity couponLog,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		return service.query(couponLog, pageable);
	}

}
