package com.yumo.kangchenjunga.coupon.v1;

import java.util.List;

public final class VerifyCouponVo {

	public CouponLogEntity verify;
	public List<CouponLogEntity> others;

	public VerifyCouponVo(CouponLogEntity verify, List<CouponLogEntity> others) {
		this.verify = verify;
		this.others = others;
	}

}
