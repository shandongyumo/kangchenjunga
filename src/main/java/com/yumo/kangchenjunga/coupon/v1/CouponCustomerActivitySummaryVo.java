package com.yumo.kangchenjunga.coupon.v1;

public final class CouponCustomerActivitySummaryVo {

	public final int customerId;
	public final int activityId;
	// 累计发放张数（包括 obtainedCoupons 的值在内）
	public final int coupons;
	// 累计领取张数（包括 usedCoupons 的值在内）
	public final int obtainedCoupons;
	// 累计使用张数
	public final int usedCoupons;

	public CouponCustomerActivitySummaryVo(int customerId, int activityId, int coupons, int obtainedCoupons,
			int usedCoupons) {
		this.customerId = customerId;
		this.activityId = activityId;
		this.coupons = coupons;
		this.obtainedCoupons = obtainedCoupons;
		this.usedCoupons = usedCoupons;
	}

}
