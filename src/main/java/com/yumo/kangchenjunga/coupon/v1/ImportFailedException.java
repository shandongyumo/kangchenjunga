package com.yumo.kangchenjunga.coupon.v1;

public final class ImportFailedException extends RuntimeException {

	private static final long serialVersionUID = -6205018108456605756L;

	public final ImportResult result;

	public ImportFailedException(ImportResult result) {
		this.result = result;
	}

}
