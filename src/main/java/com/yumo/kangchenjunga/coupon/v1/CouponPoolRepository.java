package com.yumo.kangchenjunga.coupon.v1;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.yumo.kangchenjunga.activity.Activity;

public interface CouponPoolRepository extends JpaRepository<CouponPoolEntity, Integer> {

	long countByActivityIdAndStatus(int activityId, CouponStatus status);

	List<CouponPoolEntity> findByActivityIdAndStatus(int activityId, CouponStatus unallocated, Pageable page);

	Page<CouponPoolEntity> findByStatus(CouponStatus status, Pageable pageable);

	List<CouponPoolEntity> findByActivity(Activity activity);

	boolean existsByActivity(Activity activity);

}
