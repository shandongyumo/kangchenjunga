package com.yumo.kangchenjunga.coupon.v1;

public final class AllocateResultVo {

	public final int customerAmount;
	public final int couponPerCustomer;
	public final int couponAmount;

	public AllocateResultVo(int customerAmount, int couponPerCustomer, int couponAmount) {
		this.customerAmount = customerAmount;
		this.couponPerCustomer = couponPerCustomer;
		this.couponAmount = couponAmount;
	}

}
