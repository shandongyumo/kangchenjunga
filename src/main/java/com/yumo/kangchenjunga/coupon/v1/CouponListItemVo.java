package com.yumo.kangchenjunga.coupon.v1;

import java.time.Instant;
import java.time.LocalDate;

public final class CouponListItemVo {

	public Integer couponId;
	public Integer activityId;
	public String activityTitle;
	public String couponCode;
	public String secretCode;
	public Integer termByMinute;
	public CreateType createType;
	public Instant createTime;
	public CouponStatus poolStatus;

	public Integer customerId;
	public String customerNumber;
	public String customerNickname;
	public Integer customerLevel;
	public Instant allocateTime;
	public CouponStatus allocationStatus;

	public Instant obtainTime;
	public LocalDate effectiveTime;
	public LocalDate expirationTime;
	public Instant usedTime;
	public CouponStatus obtainStatus;

	public Integer companyId;
	public String companyName;
	public Integer storeId;
	public String storeName;

}
