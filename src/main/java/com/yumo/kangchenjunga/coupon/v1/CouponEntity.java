package com.yumo.kangchenjunga.coupon.v1;

import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.yumo.kangchenjunga.activity.Activity;
import com.yumo.kangchenjunga.customer.Customer;

@Entity(name = "Coupon")
@Table(name = "coupon")
public class CouponEntity {

	@Column(name = "coupon_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;
	@OneToOne
	@JoinColumn(name = "activity_id")
	public Activity activity;
	@OneToOne
	@JoinColumn(name = "customer_id")
	public Customer customer;
	public String couponCode;
	public Instant createTime;
	public LocalDate effectiveTime;
	public LocalDate expirationTime;
	@Enumerated(EnumType.STRING)
	public Status status;

	public int getId() {
		return id;
	}

	public static CouponEntity create(CouponRepository repository, CouponEntity coupon) {
		coupon.id = 0;

		return repository.save(coupon);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public CouponEntity() {
		// Empty.
	}

	public CouponEntity(Activity activity, Customer customer, String couponCode, Instant createTime,
			LocalDate effectiveTime, LocalDate expirationTime) {
		this.activity = activity;
		this.customer = customer;
		this.couponCode = couponCode;
		this.createTime = createTime;
		this.effectiveTime = effectiveTime;
		this.expirationTime = expirationTime;
		this.status = Status.VALID;
	}

}
