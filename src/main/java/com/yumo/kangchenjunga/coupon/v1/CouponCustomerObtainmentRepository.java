package com.yumo.kangchenjunga.coupon.v1;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.yumo.kangchenjunga.activity.Activity;
import com.yumo.kangchenjunga.customer.Customer;

public interface CouponCustomerObtainmentRepository extends JpaRepository<CouponCustomerObtainmentEntity, Integer> {

	Page<CouponCustomerObtainmentEntity> findByStatus(CouponStatus status, Pageable pageable);

	Optional<CouponCustomerObtainmentEntity> findByActivityIdAndCouponCodeAndSecretCode(int activityId,
			String couponCode, String secretCode);

	Optional<CouponCustomerObtainmentEntity> findByActivityIdAndCouponCode(int activityId, String couponCode);

	List<CouponCustomerObtainmentEntity> findByActivityAndCustomerAndStatusAndCouponCodeNot(Activity activity,
			Customer customer, CouponStatus status, String couponCode);

	List<CouponCustomerObtainmentEntity> findByCouponCodeIn(Collection<String> couponCodes);

}
