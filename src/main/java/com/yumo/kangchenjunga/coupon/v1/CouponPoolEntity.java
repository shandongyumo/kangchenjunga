package com.yumo.kangchenjunga.coupon.v1;

import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.yumo.kangchenjunga.activity.Activity;

@Entity(name = "CouponPool")
@Table(name = "coupon_pool")
public final class CouponPoolEntity {

	@Column(name = "coupon_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;

	@OneToOne
	@JoinColumn(name = "activity_id")
	public Activity activity;
	public String couponCode;
	public String secretCode;
	public int termByMinute;
	@Enumerated(EnumType.STRING)
	public CreateType createType;
	public Instant createTime;
	public LocalDate effectiveTime;
	public LocalDate expirationTime;
	@Enumerated(EnumType.STRING)
	public CouponStatus status;

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static CouponPoolEntity create(CouponPoolRepository repository, CouponPoolEntity coupon) {
		coupon.id = 0;

		return repository.save(coupon);
	}

}
