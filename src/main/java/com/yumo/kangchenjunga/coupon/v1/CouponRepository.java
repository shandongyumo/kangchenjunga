package com.yumo.kangchenjunga.coupon.v1;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.yumo.kangchenjunga.activity.Activity;
import com.yumo.kangchenjunga.customer.Customer;

public interface CouponRepository extends JpaRepository<CouponEntity, Integer> {

	List<CouponEntity> findByActivity(Activity activity);

	Optional<CouponEntity> findByCouponCodeAndStatus(String couponCode, Status status);

	List<CouponEntity> findByCouponCodeIn(List<String> couponCode);

	List<CouponEntity> findByActivityAndCustomerAndStatusAndCouponCodeNot(Activity activity, Customer customer,
			Status status, String couponCode);

	boolean existsByActivity(Activity activity);

	@Modifying(clearAutomatically = true, flushAutomatically = true)
	@Query("update Coupon set status = 'IGNORED' where activity = ?1 and status = 'VALID'")
	int updateCouponStatusFromValidToIgnoredByActivityId(Activity activityId);

}
