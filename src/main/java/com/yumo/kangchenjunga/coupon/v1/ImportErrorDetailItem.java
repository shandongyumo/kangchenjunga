package com.yumo.kangchenjunga.coupon.v1;

public final class ImportErrorDetailItem {

	public final String reason;
	public final int rowNum;
	public final int colNum;
	public final String value;

	public ImportErrorDetailItem(String reason, int rowNum, int colNum, String value) {
		this.reason = reason;
		this.rowNum = rowNum;
		this.colNum = colNum;
		this.value = value;
	}

}
