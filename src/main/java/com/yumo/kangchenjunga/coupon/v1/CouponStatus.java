package com.yumo.kangchenjunga.coupon.v1;

public enum CouponStatus {
	UNALLOCATED,
	ALLOCATED,
	OBTAINED,
	USED,
	INVALID,
}
