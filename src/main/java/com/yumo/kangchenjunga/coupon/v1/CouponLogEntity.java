package com.yumo.kangchenjunga.coupon.v1;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.leafcraft.utils.webutil.datetime.DateTimeUtils;

@Entity(name = "CouponLog")
@Table(name = "coupon_log")
public class CouponLogEntity {

	@Column(name = "coupon_log_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;
	public int activityId;
	public String activityTitle;
	public int customerId;
	public String customerNumber;
	public int companyId;
	public String companyName;
	public int storeId;
	public String storeName;
	public String inputCouponCode;
	public String inputSecretCode;
	public String op;
	public Integer opUserId;
	public String opUsername;
	public Instant opTime;
	public String opResult;
	public String opResultDetail;
	public int couponId;
	public String remark;

	public CouponLogEntity() {
		// Empty.
	}

	@Deprecated
	public CouponLogEntity(int activityId, String activityTitle, int customerId, String customerNumber, int companyId,
			String companyName, String inputCouponCode, String op, Integer opUserId, String opUsername, String opStore,
			String opResult, String opResultDetail, int couponId, String remark) {
		this.activityId = activityId;
		this.activityTitle = activityTitle;
		this.customerId = customerId;
		this.customerNumber = customerNumber;
		this.companyId = companyId;
		this.companyName = companyName;
		this.inputCouponCode = inputCouponCode;
		this.op = op;
		this.opUserId = opUserId;
		this.opUsername = opUsername;
		this.opResult = opResult;
		this.opResultDetail = opResultDetail;
		this.couponId = couponId;
		this.remark = remark;
		this.opTime = DateTimeUtils.instantNow();
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private int activityId;
		private String activityTitle;
		private int customerId;
		private String customerNumber;
		private int companyId;
		private String companyName;
		private int storeId;
		private String storeName;
		private String inputCouponCode;
		private String inputSecretCode;
		private String op;
		private Integer opUserId;
		private String opUsername;
		private Instant opTime;
		private String opResult;
		private String opResultDetail;
		private int couponId;
		private String remark;

		public Builder activity(int activityId, String activityTitle) {
			this.activityId = activityId;
			this.activityTitle = activityTitle;

			return this;
		}

		public Builder customer(int customerId, String customerNumber) {
			this.customerId = customerId;
			this.customerNumber = customerNumber;

			return this;
		}

		public Builder store(int companyId, String companyName, int storeId, String storeName) {
			this.companyId = companyId;
			this.companyName = companyName;
			this.storeId = storeId;
			this.storeName = storeName;

			return this;
		}

		public Builder input(String couponCode, String secretCode) {
			this.inputCouponCode = couponCode;
			this.inputSecretCode = secretCode;

			return this;
		}

		public Builder op(String op, int opUserId, String opUsername, Instant opTime, String opResult,
				String opResultDetail, String remark) {
			this.op = op;
			this.opUserId = opUserId;
			this.opUsername = opUsername;
			this.opTime = opTime;
			this.opResult = opResult;
			this.opResultDetail = opResultDetail;
			this.remark = remark;

			return this;
		}

		public Builder coupon(int couponId) {
			this.couponId = couponId;

			return this;
		}

		public CouponLogEntity build() {
			var entity = new CouponLogEntity();

			entity.activityId = this.activityId;
			entity.activityTitle = this.activityTitle;
			entity.companyId = this.companyId;
			entity.companyName = this.companyName;
			entity.couponId = this.couponId;
			entity.customerId = this.customerId;
			entity.customerNumber = this.customerNumber;
			entity.inputCouponCode = this.inputCouponCode;
			entity.inputSecretCode = this.inputSecretCode;
			entity.op = this.op;
			entity.opResult = this.opResult;
			entity.opResultDetail = this.opResultDetail;
			entity.opTime = this.opTime;
			entity.opUserId = this.opUserId;
			entity.opUsername = this.opUsername;
			entity.remark = this.remark;
			entity.storeId = this.storeId;
			entity.storeName = this.storeName;

			return entity;
		}
	}

}
