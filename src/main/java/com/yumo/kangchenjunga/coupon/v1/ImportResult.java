package com.yumo.kangchenjunga.coupon.v1;

import java.io.Serializable;
import java.util.List;

public final class ImportResult implements Serializable {

	private static final long serialVersionUID = -2001019056292175701L;

	public final String status;
	public final List<ImportErrorDetailItem> errorDetails;

	public ImportResult(String status, List<ImportErrorDetailItem> errorDetails) {
		this.status = status;
		this.errorDetails = errorDetails;
	}

}
