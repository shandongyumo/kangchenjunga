package com.yumo.kangchenjunga.coupon.v1;

public enum CreateType {
	GENERATED,
	IMPORTED,
}
