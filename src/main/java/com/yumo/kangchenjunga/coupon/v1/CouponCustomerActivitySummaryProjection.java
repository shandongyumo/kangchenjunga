package com.yumo.kangchenjunga.coupon.v1;

public interface CouponCustomerActivitySummaryProjection {

	int getCustomerId();

	int getActivityId();

	int getCoupons();

	int getObtainedCoupons();

	int getUsedCoupons();

}
