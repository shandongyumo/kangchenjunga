package com.yumo.kangchenjunga.coupon.v1;

import java.util.List;

public final class CouponCustomerSummaryVo {

	public final int customerId;
	// 累计发放张数（包括 totalObtainedCoupons 的值在内）
	public final int totalCoupons;
	// 累计领取张数（包括 totalUsedCoupons 的值在内）
	public final int totalObtainedCoupons;
	// 累计使用张数
	public final int totalUsedCoupons;
	public final int totalActivity;

	public final List<CouponCustomerActivitySummaryVo> details;

	public CouponCustomerSummaryVo(int customerId, int totalCoupons, int totalObtainedCoupons, int totalUsedCoupons,
			int totalActivity, List<CouponCustomerActivitySummaryVo> details) {
		this.customerId = customerId;
		this.totalCoupons = totalCoupons;
		this.totalObtainedCoupons = totalObtainedCoupons;
		this.totalUsedCoupons = totalUsedCoupons;
		this.totalActivity = totalActivity;
		this.details = details;
	}

}
