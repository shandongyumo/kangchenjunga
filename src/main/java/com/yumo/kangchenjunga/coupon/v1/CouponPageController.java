package com.yumo.kangchenjunga.coupon.v1;

import java.time.LocalDate;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yumo.kangchenjunga.activity.Activity;
import com.yumo.kangchenjunga.activity.ActivityService;
import com.yumo.kangchenjunga.company.CompanyEntity;
import com.yumo.kangchenjunga.company.CompanyService;
import com.yumo.kangchenjunga.company.CompanyVo;
import com.yumo.kangchenjunga.customer.Customer;
import com.yumo.kangchenjunga.customer.CustomerService;
import com.yumo.kangchenjunga.customer.CustomerVo;
import com.yumo.kangchenjunga.security.CustomUserDetails;

@Controller
@RequestMapping("")
public class CouponPageController {

	@Autowired
	private CouponService couponService;
	@Autowired
	private CouponLogService couponLogService;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private CouponListService couponListService;

	@GetMapping("/admin/coupons")
	public String list(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CouponEntity coupon = new CouponEntity();
		coupon.activity = new Activity();
		coupon.activity.setId(StringUtils.isBlank(request.getParameter("activityId")) ? null
				: Integer.valueOf(request.getParameter("activityId")));
		coupon.customer = new Customer();
		coupon.customer.customerNumber = request.getParameter("customerNumber");
		coupon.status = StringUtils.isBlank(request.getParameter("status")) ? null
				: Enum.valueOf(Status.class, request.getParameter("status"));
		if (request.getParameter("effectiveTime") != null && !"".equals(request.getParameter("effectiveTime"))) {
			coupon.effectiveTime = LocalDate.parse(request.getParameter("effectiveTime"));
		}
		if (request.getParameter("expirationTime") != null && !"".equals(request.getParameter("expirationTime"))) {
			coupon.expirationTime = LocalDate.parse(request.getParameter("expirationTime"));
		}
		model.addAttribute("coupons", couponService.query(coupon, pageable));
		model.addAttribute("activities", activityService.list());
		model.addAttribute("search", coupon);
		return "coupon_list";
	}
	
	@GetMapping("/admin/coupons/pools")
	public String couponPools(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CouponListItemVo coupon = new CouponListItemVo();
		coupon.activityId = StringUtils.isBlank(request.getParameter("activityId")) ? null
				: Integer.valueOf(request.getParameter("activityId"));
		coupon.couponCode = StringUtils.isBlank(request.getParameter("couponCode")) ? null : request.getParameter("couponCode");
//		coupon.customerNumber = request.getParameter("customerNumber");
		model.addAttribute("coupons", couponListService.poolQuery(coupon, pageable));
		model.addAttribute("activities", activityService.list());
		model.addAttribute("search", coupon);
		return "admin/coupon_pool";
	}
	
	@GetMapping("/admin/coupons/allocations")
	public String couponAllocations(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CouponListItemVo coupon = new CouponListItemVo();
		coupon.activityId = StringUtils.isBlank(request.getParameter("activityId")) ? null
				: Integer.valueOf(request.getParameter("activityId"));
		coupon.couponCode = StringUtils.isBlank(request.getParameter("couponCode")) ? null : request.getParameter("couponCode");
//		coupon.customerNumber = request.getParameter("customerNumber");
		model.addAttribute("coupons", couponListService.allocationQuery(coupon, pageable));
		model.addAttribute("activities", activityService.list());
		model.addAttribute("search", coupon);
		return "coupon_allocation";
	}
	
	@GetMapping("/admin/coupons/obtainments")
	public String couponObtainments(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CouponListItemVo coupon = new CouponListItemVo();
		coupon.activityId = StringUtils.isBlank(request.getParameter("activityId")) ? null
				: Integer.valueOf(request.getParameter("activityId"));
		coupon.couponCode = StringUtils.isBlank(request.getParameter("couponCode")) ? null : request.getParameter("couponCode");
//		coupon.customerNumber = request.getParameter("customerNumber");
		model.addAttribute("coupons", couponListService.obtainmentQuery(coupon, pageable));
		model.addAttribute("activities", activityService.list());
		model.addAttribute("search", coupon);
		return "admin/coupon_obtainment";
	}
	
	@GetMapping("/admin/coupons/useds")
	public String couponUseds(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CouponListItemVo coupon = new CouponListItemVo();
		Integer activityId = StringUtils.isBlank(request.getParameter("activityId")) ? null
				: Integer.valueOf(request.getParameter("activityId"));
		coupon.couponCode = StringUtils.isBlank(request.getParameter("couponCode")) ? null : request.getParameter("couponCode");
//		coupon.customerNumber = request.getParameter("customerNumber");
		coupon.activityId = activityId;
		model.addAttribute("coupons", couponListService
				.usedSearch(activityId, null, null, coupon.couponCode,
						StringUtils.isBlank(request.getParameter("startDate")) ? null : LocalDate.parse(request.getParameter("startDate")), 
						StringUtils.isBlank(request.getParameter("endDate")) ? null : LocalDate.parse(request.getParameter("endDate")), 
						pageable));
		model.addAttribute("activities", activityService.list());
		model.addAttribute("search", coupon);
		return "admin/coupon_used";
	}
	
	@GetMapping("/company/coupons/useds")
	public String companyCouponUseds(@AuthenticationPrincipal CustomUserDetails currentUser, Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CouponListItemVo coupon = new CouponListItemVo();
		coupon.activityId = StringUtils.isBlank(request.getParameter("activityId")) ? null
				: Integer.valueOf(request.getParameter("activityId"));
		coupon.storeId = StringUtils.isBlank(request.getParameter("storeId")) ? null
				: Integer.valueOf(request.getParameter("storeId"));
		coupon.couponCode = StringUtils.isBlank(request.getParameter("couponCode")) ? null : request.getParameter("couponCode");
//		coupon.customerNumber = request.getParameter("customerNumber");
		model.addAttribute("coupons", couponListService
				.usedSearch(coupon.activityId, currentUser.getCompanyId(), 
						currentUser.getOpStoreId() == 0 ? coupon.storeId : currentUser.getOpStoreId(), coupon.couponCode, 
						StringUtils.isBlank(request.getParameter("startDate")) ? null : LocalDate.parse(request.getParameter("startDate")), 
						StringUtils.isBlank(request.getParameter("endDate")) ? null : LocalDate.parse(request.getParameter("endDate")), 
						pageable));
		model.addAttribute("activities", activityService.listByCompanyId(currentUser.getCompanyId()));
		model.addAttribute("stores", companyService.queryStoreByCompanyId(currentUser.getCompanyId()));
		model.addAttribute("search", coupon);
		return "company/coupon_used";
	}
	
	@GetMapping("/admin/coupons/allocate/customerLevel")
	public String allocateByCustomerLevel(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		Activity activity = new Activity();
		CompanyVo companyVo = new CompanyVo();

		activity.title = request.getParameter("title");
		companyVo.id = Optional.ofNullable(StringUtils.trimToNull(request.getParameter("companyId")))
				.map(Integer::valueOf).orElse(null);
		activity.company = new CompanyEntity(companyVo);

		model.addAttribute("activities", activityService.query(activity, pageable));
		model.addAttribute("companies", companyService.list());
		model.addAttribute("search", activity);
		return "admin/coupon_allocate_by_customer_level";
	}
	
	@GetMapping("/admin/coupons/allocate/customer")
	public String allocateByCustomer(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		CustomerVo customer = new CustomerVo();
		customer.customerNumber = StringUtils.isBlank(request.getParameter("customerNumber")) ? null
				: request.getParameter("customerNumber");
		customer.nickname = StringUtils.isBlank(request.getParameter("nickname")) ? null
				: request.getParameter("nickname");
		customer.phone = StringUtils.isBlank(request.getParameter("phone")) ? null : request.getParameter("phone");
		customer.customerLevel = StringUtils.isBlank(request.getParameter("customerLevel")) ? null
				: Integer.valueOf(request.getParameter("customerLevel"));
		model.addAttribute("customers", customerService.query(customer, pageable));
		model.addAttribute("activities", activityService.list());
		model.addAttribute("search", customer);
		return "admin/coupon_allocate_by_customer";
	}

	@GetMapping("/company/coupons/validate")
	public String validate(@AuthenticationPrincipal CustomUserDetails currentUser, Model model) {
		model.addAttribute("activities", activityService.listByCompanyId(currentUser.getCompanyId()));
		return "company/validate";
	}

	@GetMapping("/admin/coupons/history")
	public String history(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		model.addAttribute("couponLogs", couponLogService.query(
				StringUtils.isBlank(request.getParameter("activityId")) ? null
						: Integer.valueOf(request.getParameter("activityId")),
				null,
				StringUtils.isBlank(request.getParameter("startDate")) ? null
						: LocalDate.parse(request.getParameter("startDate")),
				StringUtils.isBlank(request.getParameter("endDate")) ? null
						: LocalDate.parse(request.getParameter("endDate")),
				pageable));
		model.addAttribute("activities", activityService.list());
		return "coupon_history";
	}

	@GetMapping("/company/coupons/history")
	public String companyHistory(@AuthenticationPrincipal CustomUserDetails currentUser, Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		model.addAttribute("couponLogs", couponLogService.query(
				StringUtils.isBlank(request.getParameter("activityId")) ? null
						: Integer.valueOf(request.getParameter("activityId")),
				currentUser.getCompanyId(),
				StringUtils.isBlank(request.getParameter("startDate")) ? null
						: LocalDate.parse(request.getParameter("startDate")),
				StringUtils.isBlank(request.getParameter("endDate")) ? null
						: LocalDate.parse(request.getParameter("endDate")),
				pageable));
		model.addAttribute("activities", activityService.listByCompanyId(currentUser.getCompanyId()));
		return "company/coupon_history";
	}

}
