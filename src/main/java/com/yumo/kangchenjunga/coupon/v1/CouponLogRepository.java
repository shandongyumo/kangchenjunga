package com.yumo.kangchenjunga.coupon.v1;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CouponLogRepository extends JpaRepository<CouponLogEntity, Integer>, QuerydslPredicateExecutor<CouponLogEntity> {
	// Empty.
}
