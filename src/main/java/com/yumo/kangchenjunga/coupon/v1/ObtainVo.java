package com.yumo.kangchenjunga.coupon.v1;

import java.time.LocalDate;

public final class ObtainVo {

	public final int customerId;
	public final int activityId;
	public final String couponCode;
	public final String secretCode;
	public final LocalDate effectiveTime;
	public final LocalDate expirationTime;

	public ObtainVo(int customerId, int activityId, String couponCode, String secretCode, LocalDate effectiveTime,
			LocalDate expirationTime) {
		this.customerId = customerId;
		this.activityId = activityId;
		this.couponCode = couponCode;
		this.secretCode = secretCode;
		this.effectiveTime = effectiveTime;
		this.expirationTime = expirationTime;
	}

}
