package com.yumo.kangchenjunga.coupon.v1;

import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.yumo.kangchenjunga.activity.Activity;
import com.yumo.kangchenjunga.customer.Customer;

@Entity(name = "CouponCustomerObtainment")
@Table(name = "coupon_customer_obtainment")
public final class CouponCustomerObtainmentEntity {

	@Column(name = "obtainment_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;

	public int couponId;
	@OneToOne
	@JoinColumn(name = "activity_id")
	public Activity activity;
	@OneToOne
	@JoinColumn(name = "customer_id")
	public Customer customer;
	public String couponCode;
	public String secretCode;
	public Instant obtainTime;
	public LocalDate effectiveTime;
	public LocalDate expirationTime;
	public Instant usedTime;
	@Enumerated(EnumType.STRING)
	public CouponStatus status;

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static CouponCustomerObtainmentEntity create(CouponCustomerObtainmentRepository repository, CouponCustomerObtainmentEntity coupon) {
		coupon.id = 0;

		return repository.save(coupon);
	}

}
