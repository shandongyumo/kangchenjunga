package com.yumo.kangchenjunga.coupon.v2;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yumo.kangchenjunga.company.NoSuchCompanyException;
import com.yumo.kangchenjunga.company.NoSuchStoreException;
import com.yumo.kangchenjunga.coupon.v1.CouponListItemVo;
import com.yumo.kangchenjunga.coupon.v1.CouponListService;
import com.yumo.kangchenjunga.coupon.v1.CouponLogEntity;
import com.yumo.kangchenjunga.coupon.v1.VerifyCouponVo;
import com.yumo.kangchenjunga.exception.controller.HttpNotFoundException;
import com.yumo.kangchenjunga.security.CustomUserDetails;
import com.yumo.kangchenjunga.user.NoSuchUserException;

@RestController("couponControllerV2")
@RequestMapping("/api/v2/coupons")
public final class CouponController {

	@Autowired
	private CouponService service;
	@Autowired
	private CouponListService couponListService;

	@PostMapping("/{couponCode}/verify")
	public VerifyCouponVo verify(@PathVariable String couponCode, @RequestParam String secretCode,
			@RequestParam int activityId, @AuthenticationPrincipal CustomUserDetails user)
			throws HttpNotFoundException {
		try {
			return service.verify(activityId, couponCode, secretCode, user.getOpStoreId(), user.getId());
		} catch (NoSuchUserException e) {
			throw HttpNotFoundException.build(e, "用户 [ id: %d ] 不存在。", user.getId());
		} catch (NoSuchStoreException e) {
			throw HttpNotFoundException.build(e, "门店 [ id: %d ] 不存在。", user.getOpStoreId());
		} catch (NoSuchCompanyException e) {
			throw HttpNotFoundException.build(e, "企业 [ id: %d ] 不存在。", user.getCompanyId());
		}
	}

	@PostMapping("/services/redeem")
	public List<CouponLogEntity> redeem(@RequestParam String remark, @RequestBody List<String> couponCodes,
			@RequestParam int activityId, @AuthenticationPrincipal CustomUserDetails user)
			throws HttpNotFoundException {
		try {
			return service.redeem(activityId, couponCodes, user.getOpStoreId(), user.getId(), remark);
		} catch (NoSuchUserException e) {
			throw HttpNotFoundException.build(e, "用户 [ id: %d ] 不存在。", user.getId());
		} catch (NoSuchStoreException e) {
			throw HttpNotFoundException.build(e, "门店 [ id: %d ] 不存在。", user.getOpStoreId());
		} catch (NoSuchCompanyException e) {
			throw HttpNotFoundException.build(e, "企业 [ id: %d ] 不存在。", user.getCompanyId());
		}
	}

	@PostMapping("/services/redeem2")
	public RedeemResponseVo redeem(@RequestBody RedeemRequestVo vo) {
		return service.redeem(vo);
	}

	@PostMapping("/services/poolQuery")
	public Page<CouponListItemVo> poolQuery(@RequestBody CouponListItemVo coupon,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		return couponListService.poolQuery(coupon, pageable);
	}

	@PostMapping("/services/obtainmentQuery")
	public Page<CouponListItemVo> obtainmentQuery(@RequestBody CouponListItemVo coupon,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		return couponListService.obtainmentQuery(coupon, pageable);
	}

	@GetMapping("/services/usedQuery")
	public Page<CouponListItemVo> usedQuery(HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable,
			@AuthenticationPrincipal CustomUserDetails user) {
		Integer companyId = null;
		Integer storeId = null;

		if (user.getCompanyId() != null && user.getCompanyId() > 1) {
			companyId = user.getCompanyId();
		}

		if (user.getOpStoreId() != null && user.getOpStoreId() != 0) {
			storeId = user.getOpStoreId();
		}

		return couponListService
				.usedSearch(
						StringUtils.isBlank(request.getParameter("activityId")) ? null
								: Integer.valueOf(request.getParameter("activityId")),
						companyId, storeId,
						StringUtils.isBlank(request.getParameter("couponCode")) ? null
								: request.getParameter("couponCode"),
						StringUtils.isBlank(request.getParameter("startDate")) ? null
								: LocalDate.parse(request.getParameter("startDate")),
						StringUtils.isBlank(request.getParameter("endDate")) ? null
								: LocalDate.parse(request.getParameter("endDate")),
						pageable);
	}

}
