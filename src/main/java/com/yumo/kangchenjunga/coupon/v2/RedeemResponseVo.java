package com.yumo.kangchenjunga.coupon.v2;

import java.util.List;

public final class RedeemResponseVo {

	public final int activityId;
	public final String activityTitle;
	public final int customerId;
	public final String customerNumber;
	public final int companyId;
	public final String companyName;
	public final List<RedeemCouponVo> coupons;

	public RedeemResponseVo(int activityId, String activityTitle, int customerId, String customerNumber, int companyId,
			String companyName, List<RedeemCouponVo> coupons) {
		this.activityId = activityId;
		this.activityTitle = activityTitle;
		this.customerId = customerId;
		this.customerNumber = customerNumber;
		this.companyId = companyId;
		this.companyName = companyName;
		this.coupons = coupons;
	}

}
