package com.yumo.kangchenjunga.coupon.v2;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class RedeemRequestVo {

	public final int storeId;
	public final int userId;
	public final List<RedeemCouponVo> coupons;

	@JsonCreator
	public RedeemRequestVo(@JsonProperty("storeId") int storeId, @JsonProperty("userId") int userId,
			@JsonProperty("coupons") List<RedeemCouponVo> coupons) {
		this.storeId = storeId;
		this.userId = userId;
		this.coupons = coupons;
	}

}
