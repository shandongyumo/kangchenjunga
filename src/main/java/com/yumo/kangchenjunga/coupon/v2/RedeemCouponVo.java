package com.yumo.kangchenjunga.coupon.v2;

import com.fasterxml.jackson.annotation.JsonCreator;

public final class RedeemCouponVo {

	public final String couponCode;
	public final String secretCode;

	@JsonCreator
	public RedeemCouponVo(String couponCode, String secretCode) {
		this.couponCode = couponCode;
		this.secretCode = secretCode;
	}

}