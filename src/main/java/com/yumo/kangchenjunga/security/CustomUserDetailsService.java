package com.yumo.kangchenjunga.security;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.yumo.kangchenjunga.user.NoSuchUserException;
import com.yumo.kangchenjunga.user.User;
import com.yumo.kangchenjunga.user.UserService;

public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String paramStr) throws UsernameNotFoundException {
		String[] params = paramStr.split(CustomUsernamePasswordAuthenticationFilter.SPLIT);
		String companyId = "";
		String username = "";
		if(params.length > 1) {
			companyId = params[0];
			username = params[1];
		} else {
			username = paramStr;
		}
		User user = new User();
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
		try {
			user = userService.query(username, StringUtils.isBlank(companyId) ? 1 : Integer.valueOf(companyId));

			if(user.getId() == 0) {
				throw new UsernameNotFoundException("username " + username + " not found");
			}

			if(!StringUtils.isBlank(companyId)) {
				if(!StringUtils.equals(user.company.getId().toString(), companyId)) {
					throw new UsernameNotFoundException("User " + username + " has no GrantedAuthority");
				}
			}

			if(user.privilege != null) {

				switch (user.privilege) {
				case "admin":
					authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
					break;
				case "power_user":
					authorities.add(new SimpleGrantedAuthority("ROLE_POVER_USER"));
					break;
				case "user":
					authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
					break;
				case "company_admin":
					authorities.add(new SimpleGrantedAuthority("ROLE_COMPANY_ADMIN"));
					break;
				case "company_power_user":
					authorities.add(new SimpleGrantedAuthority("ROLE_COMPANY_POWER_USER"));
					break;
				case "company_user":
					authorities.add(new SimpleGrantedAuthority("ROLE_COMPANY_USER"));
					break;
				case "store_admin":
					authorities.add(new SimpleGrantedAuthority("ROLE_STORE_ADMIN"));
					break;
				case "store_power_user":
					authorities.add(new SimpleGrantedAuthority("ROLE_STORE_POWER_USER"));
					break;
				case "store_user":
					authorities.add(new SimpleGrantedAuthority("ROLE_STORE_USER"));
					break;
				default:
					break;
				}
			}

		} catch (NoSuchUserException e) {
			e.printStackTrace();
		}
		return new CustomUserDetails(user.getId(),
				"".equals(companyId) ? 1 : Integer.valueOf(companyId),
				user.store == null ? 0 : user.store.getId(),
				user.username, user.password, authorities);
	}

}
