package com.yumo.kangchenjunga.exception.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.yumo.kangchenjunga.exception.InvalidArgumentException;

@RestControllerAdvice
public final class ExceptionController {

	@ExceptionHandler(HttpException.class)
	private void handle(HttpServletResponse response, HttpException e) throws IOException {
		response.sendError(e.getStatus(), e.getMessage());
	}

	@ExceptionHandler({ IllegalArgumentException.class, InvalidArgumentException.class })
	private void handle(HttpServletResponse response, RuntimeException e) throws IOException {
		response.sendError(400, e.getMessage());
	}

}
