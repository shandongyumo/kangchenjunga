package com.yumo.kangchenjunga.exception.controller;

public class HttpException extends Exception {

	private static final long serialVersionUID = 6487789599091290922L;

	private final int status;

	public HttpException(String message, Throwable cause, int status) {
		super(message, cause);

		this.status = status;
	}

	public HttpException(String message, int status) {
		super(message);

		this.status = status;
	}

	public HttpException(Throwable cause, int status) {
		super(cause);

		this.status = status;
	}

	public int getStatus() {
		return status;
	}

}
