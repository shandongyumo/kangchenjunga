package com.yumo.kangchenjunga.exception.controller;

import org.springframework.http.HttpStatus;

public final class HttpNotFoundException extends HttpException {

	private static final long serialVersionUID = 4912812217351867133L;

	private HttpNotFoundException(String message, Throwable cause) {
		super(message, cause, HttpStatus.NOT_FOUND.value());
	}

	private HttpNotFoundException(String message) {
		super(message, HttpStatus.NOT_FOUND.value());
	}

	private HttpNotFoundException(Throwable cause) {
		super(cause, HttpStatus.NOT_FOUND.value());
	}

	public static HttpNotFoundException build(Throwable cause, String message, Object... args) {
		return new HttpNotFoundException(String.format(message, args), cause);
	}

	public static HttpNotFoundException build(String message, Object... args) {
		return new HttpNotFoundException(String.format(message, args));
	}

	public static HttpNotFoundException build(Throwable cause) {
		return new HttpNotFoundException(cause);
	}

	public static void buildAndthrow(Throwable cause, String message, Object... args) throws HttpNotFoundException {
		throw new HttpNotFoundException(String.format(message, args), cause);
	}

	public static void buildAndthrow(String message, Object... args) throws HttpNotFoundException {
		throw new HttpNotFoundException(String.format(message, args));
	}

	public static void buildAndthrow(Throwable cause) throws HttpNotFoundException {
		throw new HttpNotFoundException(cause);
	}

}
