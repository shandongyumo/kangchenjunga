package com.yumo.kangchenjunga.exception.controller;

import org.springframework.http.HttpStatus;

public final class HttpBadRequestException extends HttpException {

	private static final long serialVersionUID = 2165548071114807903L;

	private HttpBadRequestException(String message, Throwable cause) {
		super(message, cause, HttpStatus.BAD_REQUEST.value());
	}

	private HttpBadRequestException(String message) {
		super(message, HttpStatus.BAD_REQUEST.value());
	}

	private HttpBadRequestException(Throwable cause) {
		super(cause, HttpStatus.BAD_REQUEST.value());
	}

	public static HttpBadRequestException build(Throwable cause, String message, Object... args) {
		return new HttpBadRequestException(String.format(message, args), cause);
	}

	public static HttpBadRequestException build(String message, Object... args) {
		return new HttpBadRequestException(String.format(message, args));
	}

	public static HttpBadRequestException build(Throwable cause) {
		return new HttpBadRequestException(cause);
	}

	public static void buildAndthrow(Throwable cause, String message, Object... args) throws HttpBadRequestException {
		throw new HttpBadRequestException(String.format(message, args), cause);
	}

	public static void buildAndthrow(String message, Object... args) throws HttpBadRequestException {
		throw new HttpBadRequestException(String.format(message, args));
	}

	public static void buildAndthrow(Throwable cause) throws HttpBadRequestException {
		throw new HttpBadRequestException(cause);
	}

}
