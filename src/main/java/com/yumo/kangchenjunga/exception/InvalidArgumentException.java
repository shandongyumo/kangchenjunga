package com.yumo.kangchenjunga.exception;

public class InvalidArgumentException extends RuntimeException {

	private static final long serialVersionUID = 7107485668956672769L;

	public InvalidArgumentException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidArgumentException(String message) {
		super(message);
	}

	public InvalidArgumentException(Throwable cause) {
		super(cause);
	}

}
