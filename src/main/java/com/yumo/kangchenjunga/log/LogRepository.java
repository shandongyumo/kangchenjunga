package com.yumo.kangchenjunga.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface LogRepository extends JpaRepository<LogEntity, Integer>, QuerydslPredicateExecutor<LogEntity> {

}
