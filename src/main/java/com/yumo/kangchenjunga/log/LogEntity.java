package com.yumo.kangchenjunga.log;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Log")
@Table(name = "log")
public final class LogEntity {

	@Column(name = "log_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;

	public Integer activityId;
	public String activityTitle;
	public Integer companyId;
	public String companyName;
	public Integer storeId;
	public String storeName;
	public String opObject;
	public String op;
	public Integer opUserId;
	public String opUsername;
	public Instant opTime;
	public String opResult;
	public String opDetail;

	public static LogEntity create(LogRepository repository, LogEntity log) {
		log.id = 0;

		return repository.save(log);
	}

}
