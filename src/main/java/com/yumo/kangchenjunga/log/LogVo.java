package com.yumo.kangchenjunga.log;

import java.time.Instant;

public final class LogVo {

	public Integer opUserId;
	public String opUsername;
	public String opObject;
	public String op;
	public String opResult;
	public String opDetail;
	public Instant opTime;

}
