package com.yumo.kangchenjunga.log;

import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/log")
public final class LogController {

	@Autowired
	private LogService logService;
	
	@GetMapping("/services/query")
	public Page<LogVo> list(Model model, HttpServletRequest request,
			@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable pageable) {
		return logService.search(
				StringUtils.isBlank(request.getParameter("activityId")) ? null : Integer.valueOf(request.getParameter("activityId")),
						StringUtils.isBlank(request.getParameter("opObject")) ? null : request.getParameter("opObject"), 
						StringUtils.isBlank(request.getParameter("op")) ? null : request.getParameter("op"), 
						StringUtils.isBlank(request.getParameter("startDate")) ? null : LocalDate.parse(request.getParameter("startDate")), 
						StringUtils.isBlank(request.getParameter("endDate")) ? null : LocalDate.parse(request.getParameter("endDate")), 
						pageable);
	}

}
