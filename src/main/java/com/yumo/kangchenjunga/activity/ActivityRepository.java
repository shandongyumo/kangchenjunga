package com.yumo.kangchenjunga.activity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {

	@Modifying
	@Query("update from Activity set status = ?2 where id = ?1 and status = 'VALID'")
	int updateStatus(int id, Status interrupted);

	boolean existsByCompanyId(int companyId);

	List<Activity> findByCompanyId(int companyId);

}
