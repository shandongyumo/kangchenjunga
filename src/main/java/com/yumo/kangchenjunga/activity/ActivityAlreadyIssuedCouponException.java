package com.yumo.kangchenjunga.activity;

public class ActivityAlreadyIssuedCouponException extends Exception {

	private static final long serialVersionUID = -9034389980297001406L;

	public ActivityAlreadyIssuedCouponException(String message) {
		super(message);
	}

}
