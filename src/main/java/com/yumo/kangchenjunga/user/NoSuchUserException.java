package com.yumo.kangchenjunga.user;

public class NoSuchUserException extends Exception {

	private static final long serialVersionUID = 8170432588970336921L;

	public NoSuchUserException(String message) {
		super(message);
	}

}
