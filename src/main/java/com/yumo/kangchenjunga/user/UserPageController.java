package com.yumo.kangchenjunga.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yumo.kangchenjunga.company.CompanyService;
import com.yumo.kangchenjunga.company.NoSuchCompanyException;
import com.yumo.kangchenjunga.company.NoSuchStoreException;
import com.yumo.kangchenjunga.security.CustomUserDetails;

@Controller
@RequestMapping("")
public class UserPageController {

	@Autowired
	private UserService userService;
	@Autowired
	private CompanyService companyService;

	@GetMapping("/admin/users")
	public String users(Model model, @RequestParam(required = false, defaultValue = "0") int page, @RequestParam(required = false, defaultValue = "10") int size) {
		model.addAttribute("users", userService.pageByPrivilege(page, size, "admin"));
		return "admin/user";
	}

	@GetMapping("/company/users")
	public String companyUsers(@AuthenticationPrincipal CustomUserDetails currentUser, Model model,
			@RequestParam(required = false, defaultValue = "0") int page,
			@RequestParam(required = false, defaultValue = "10") int size) throws NoSuchCompanyException, NoSuchStoreException {
		model.addAttribute("users", userService.pageByCompanyId(page, size, currentUser.getCompanyId()));
		model.addAttribute("stores", companyService.queryStoreByCompanyId(currentUser.getCompanyId()));
		return "company/user";
	}

}
