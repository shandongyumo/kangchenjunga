package com.yumo.kangchenjunga.user;

import com.fasterxml.jackson.annotation.JsonView;

public final class UserVo {

	@JsonView(Output.class)
	public int id;
	@JsonView(Output.class)
	public String username;
	public String password;
	@JsonView(Output.class)
	public int companyId;
	@JsonView(Output.class)
	public String companyName;
	@JsonView(Output.class)
	public int storeId;
	@JsonView(Output.class)
	public String storeName;
	@JsonView(Output.class)
	public String privilege;

	public UserVo() {
		// Empty.
	}

	public UserVo(User user) {
		this.id = user.getId();
		this.username = user.username;
		this.privilege = user.privilege;
		this.companyId = user.company.getId();
		this.companyName = user.company.name;
		this.storeId = user.store == null ? 0 : user.store.getId();
		this.storeName = user.store == null ? "" : user.store.name;
	}

	public interface Output {
	}

}
