package com.yumo.kangchenjunga.user;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

	@Modifying
	@Query("delete #{#entityName} e where id = ?1")
	int deleteById(int id);

	Optional<User> findByUsernameAndCompanyId(String username, int companyId);

	Page<User> findByCompanyId(int companyId, Pageable pageable);

	Page<User> findByPrivilege(String privilege, Pageable pageable);

	Optional<User> findByIdAndStoreId(int userId, int storeId);

	Optional<User> findByCompanyIdAndUsername(int companyId, String adminName);

}
