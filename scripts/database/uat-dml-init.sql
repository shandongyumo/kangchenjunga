insert into company (company_id, name, contacts, phone, address)
  values
  (1, 'default', 'default', 'default', 'default')
 ,(2, 'test', 'test', 'test', 'test');

insert into user (username, salt, password, company_id, privilege)
  values
  ('admin', '3e95a635-89d8-41f6-baa5-e5e2170eacee', '52bccbac75eea7a5053166ba4044a38d039319e06dcacfd401ed54684f49cb48', 1, 'admin')
 ,('admin', '6d853138-1e9e-48f0-91b9-c2a2fe5303cf', '03de461e613b0f87f7042b68c2d9273d4a8d4fdf2cc66ef91584d14f459b36bb', 2, 'company_admin')
  ;
