insert into activity (title, content, company_id, scope, start_date, end_date, customer_levels, quantity, price)
  values
  ('中秋活动', '大闸蟹99包邮', 3, '全国所有门店', '2018-10-01 00:00:00', '2018-12-21 00:00:00', '1,2,3', 3, 50)
 ,('元旦大放送', '赠送鲁信影城观影券', 2, '济南', '2018-11-01 00:00:00', '2018-12-16 00:00:00', '3', 2, 9.9)
  ;

insert into company (company_id, name, contacts, phone, address)
  values
  (1, '鲁信', '张三', '13812345678', '山东省济南市')
 ,(2, '金拱门', '1', '13812341234', '山东省济南市历下区')
 ,(3, '开封菜', '1', '13812341235', '山东省济南市历城区');

insert into store (store_id, company_id, name, address)
  values
  (1, 2, '泉城广场店', '泉城广场')
  ;

insert into customer (customer_number, customer_level, nickname, phone)
  values
  ('XT20161026', 2, '极光', '13512345678')
 ,('XT20114210', 3, 'apple', '13512345679')
  ;

insert into user (username, salt, password, company_id, store_id, privilege)
  values
  ('admin', '3e95a635-89d8-41f6-baa5-e5e2170eacee', '52bccbac75eea7a5053166ba4044a38d039319e06dcacfd401ed54684f49cb48', 1, null, 'admin')
 ,('admin', '6d853138-1e9e-48f0-91b9-c2a2fe5303cf', '03de461e613b0f87f7042b68c2d9273d4a8d4fdf2cc66ef91584d14f459b36bb', 2, null, 'company_admin')
 ,('store1', '6d853138-1e9e-48f0-91b9-c2a2fe5303cf', '03de461e613b0f87f7042b68c2d9273d4a8d4fdf2cc66ef91584d14f459b36bb', 2, 1, 'store_user')
  ;
