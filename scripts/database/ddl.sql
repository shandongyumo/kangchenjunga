create schema @schemaname DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;

CREATE TABLE `activity` (
  `activity_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` varchar(400) DEFAULT NULL,
  `company_id` int NOT NULL,
  `scope` varchar(400) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `customer_levels` varchar(10) CHARACTER SET ascii NOT NULL,
  `quantity` int NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `status` enum('VALID','INTERRUPTED') NOT NULL,
  `remark` varchar(400) DEFAULT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB;

CREATE TABLE `activity_store_map` (
  `activity_id` int NOT NULL,
  `store_id` int NOT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`activity_id`, `store_id`)
) ENGINE=InnoDB;

CREATE TABLE `company` (
  `company_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` int DEFAULT NULL,
  `contacts` varchar(20) NOT NULL,
  `phone` varchar(20) CHARACTER SET ascii NOT NULL,
  `address` varchar(400) NOT NULL,
  `remark` varchar(400) DEFAULT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`company_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`)
) ENGINE=InnoDB;

CREATE TABLE `company_category` (
  `company_category_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `status` enum('VALID','INVALID') NOT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`company_category_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB;

CREATE TABLE `coupon` (
  `coupon_id` int NOT NULL AUTO_INCREMENT,
  `activity_id` int NOT NULL,
  `customer_id` int NOT NULL,
  `coupon_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `effective_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expiration_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('VALID','USED','IGNORED') NOT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `coupon_code_UNIQUE` (`coupon_code`)
) ENGINE=InnoDB;

CREATE TABLE `coupon_log` (
  `coupon_log_id` int NOT NULL AUTO_INCREMENT,
  `activity_id` int NOT NULL,
  `activity_title` varchar(50) NOT NULL,
  `customer_id` int NOT NULL,
  `customer_number` varchar(16) NOT NULL,
  `company_id` int NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `store_id` int NOT NULL,
  `store_name` varchar(50) NOT NULL,
  `input_coupon_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `input_secret_code` varchar(24) CHARACTER SET ascii DEFAULT NULL,
  `op` varchar(10) CHARACTER SET ascii NOT NULL COMMENT 'verify/redeem',
  `op_user_id` int NOT NULL,
  `op_username` varchar(50) NOT NULL,
  `op_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `op_result` varchar(10) CHARACTER SET ascii NOT NULL COMMENT 'success/failure',
  `op_result_detail` varchar(60) DEFAULT NULL,
  `coupon_id` int NOT NULL COMMENT '0 means no match',
  `remark` varchar(255) DEFAULT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`coupon_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `coupon_pool` (
  `coupon_id` int NOT NULL AUTO_INCREMENT,
  `activity_id` int NOT NULL,
  `coupon_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `secret_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `term_by_minute` int NOT NULL,
  `create_type` enum('GENERATED', 'IMPORTED') NOT NULL,
  `create_time` timestamp NOT NULL,
  `effective_time` timestamp NULL,
  `expiration_time` timestamp NULL,
  `status` enum('UNALLOCATED','ALLOCATED','INVALID') NOT NULL COMMENT 'UNALLOCATED 可发放, ALLOCATED 已发放, INVALID 作废',
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `activity_id_coupon_code_UNIQUE` (`activity_id`, `coupon_code`)
) ENGINE=InnoDB;

CREATE TABLE `coupon_customer_allocation` (
  `allocation_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NOT NULL,
  `coupon_id` int NOT NULL,
  `activity_id` int NOT NULL,
  `coupon_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `secret_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `term_by_minute` int NOT NULL,
  `allocate_time` timestamp NOT NULL,
  `effective_time` timestamp NULL,
  `expiration_time` timestamp NULL,
  `status` enum('ALLOCATED','OBTAINED','INVALID') NOT NULL COMMENT 'ALLOCATED 可领取, OBTAINED 已领取, INVALID 作废',
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`allocation_id`),
  INDEX `idx_customer_id` (`customer_id`),
  UNIQUE KEY `coupon_id_UNIQUE` (`coupon_id`),
  UNIQUE KEY `activity_id_coupon_code_UNIQUE` (`activity_id`, `coupon_code`)
) ENGINE=InnoDB;

CREATE TABLE `coupon_customer_obtainment` (
  `obtainment_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NOT NULL,
  `coupon_id` int NOT NULL,
  `activity_id` int NOT NULL,
  `coupon_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `secret_code` varchar(24) CHARACTER SET ascii NOT NULL,
  `obtain_time` timestamp NOT NULL,
  `effective_time` timestamp NOT NULL,
  `expiration_time` timestamp NOT NULL,
  `used_time` timestamp NULL,
  `status` enum('OBTAINED','USED','INVALID') NOT NULL COMMENT 'OBTAINED 已领取, USED 已使用, INVALID 作废',
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`obtainment_id`),
  INDEX `idx_customer_id` (`customer_id`),
  UNIQUE KEY `coupon_id_UNIQUE` (`coupon_id`),
  UNIQUE KEY `activity_id_coupon_code_UNIQUE` (`activity_id`, `coupon_code`),
  INDEX `idx_used_time` (`used_time`)
) ENGINE=InnoDB;

CREATE TABLE `customer` (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `customer_number` varchar(16) NOT NULL,
  `customer_level` int NOT NULL,
  `nickname` varchar(16) DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET ascii DEFAULT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `customer_number_UNIQUE` (`customer_number`),
  UNIQUE KEY `phone_UNIQUE` (`phone`)
) ENGINE=InnoDB;

CREATE TABLE `invoice_info` (
  `id` int NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `tax_no` varchar(30) NOT NULL,
  `address_and_phone` varchar(200) NOT NULL,
  `branch_and_account` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `log` (
  `log_id` int NOT NULL AUTO_INCREMENT,
  `activity_id` int DEFAULT NULL,
  `activity_title` varchar(50) DEFAULT NULL,
  `company_id` int DEFAULT NULL,
  `company_name` varchar(50) DEFAULT NULL,
  `store_id` int DEFAULT NULL,
  `store_name` varchar(50) DEFAULT NULL,
  `op_object` varchar(50) NOT NULL COMMENT 'activity / coupon',
  `op` varchar(10) NOT NULL COMMENT 'C / U / D',
  `op_user_id` int NOT NULL,
  `op_username` varchar(50) NOT NULL,
  `op_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `op_result` varchar(10) NOT NULL COMMENT 'success/failure',
  `op_detail` varchar(200) DEFAULT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB;

CREATE TABLE `statement_detail` (
  `statement_datail_id` int NOT NULL AUTO_INCREMENT,
  `statement_entry_id` int NOT NULL,
  `store_id` int NOT NULL,
  `store_name` varchar(50) NOT NULL,
  `activity_id` int NOT NULL,
  `activity_title` varchar(50) NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `quantity` decimal(20,6) NOT NULL,
  `amount` decimal(20,6) NOT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`statement_datail_id`),
  UNIQUE KEY `statement_entry_id_activity_id_UNIQUE` (`statement_entry_id`, `activity_id`)
) ENGINE=InnoDB;

CREATE TABLE `statement_entry` (
  `statement_entry_id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `amount` decimal(20,6) NOT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`statement_entry_id`)
) ENGINE=InnoDB;

CREATE TABLE `store` (
  `store_id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `contact_info` varchar(60) DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB;

CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(64) CHARACTER SET ascii NOT NULL,
  `company_id` int NOT NULL,
  `store_id` int DEFAULT NULL,
  `salt` varchar(40) CHARACTER SET ascii NOT NULL,
  `privilege` varchar(20) CHARACTER SET ascii NOT NULL COMMENT 'admin/power_user/user/company_admin/company_power_user/company_user/store_admin/store_power_user/store_user',
  `row_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `company_id_username_UNIQUE` (`company_id`,`username`)
) ENGINE=InnoDB;
